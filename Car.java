package exercise1;

public class Car {
	private String color;
	private int maxspeed;
	private int basicprice;
	private int basicdrain;
	private int kilometer;
	private String type;

	private Producer producer;
	private Engine engine;

	public Car(String color, int maxspeed, int basicprice, int basicdrain, int kilometer, Producer producer, Engine engine) {
		super();
		this.color = color;
		this.maxspeed = maxspeed;
		this.basicprice = basicprice;
		this.basicdrain = basicdrain;
		this.kilometer = kilometer;
		this.producer = producer;
		this.engine = engine;
	}

	public int getPrice() {
		double price = this.basicprice * (1-this.producer.getDiscount());
		return (int)price;
	}
	
	public String getType() {
		String type = this.engine.getFuel();
		return type;
	}
	
	public double getUsage() {
		getType();
		double usage = this.basicdrain;
		if(this.kilometer>=50000 && this.type=="Benzin") {
			usage = this.basicdrain*1.098;
		}		
		return usage;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getMaxspeed() {
		return maxspeed;
	}

	public void setMaxspeed(int maxspeed) {
		this.maxspeed = maxspeed;
	}

	public int getBasicprice() {
		return basicprice;
	}

	public void setBasicprice(int basicprice) {
		this.basicprice = basicprice;
	}

	public int getBasicdrain() {
		return basicdrain;
	}

	public void setBasicdrain(int basicdrain) {
		this.basicdrain = basicdrain;
	}

	public int getKilometer() {
		return kilometer;
	}

	public void setKilometer(int kilometer) {
		this.kilometer = kilometer;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

}
