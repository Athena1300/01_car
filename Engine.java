package exercise1;

public class Engine {
	private String fuel;
	private double power;
	
	public Engine(String fuel, double power) {
		super();
		this.fuel = fuel;
		this.power = power;
	}
	
	public String getFuel() {
		return fuel;
	}
	public void setFuel(String fuel) {
		this.fuel = fuel;
	}
	public double getPower() {
		return power;
	}
	public void setPower(double power) {
		this.power = power;
	}
}
