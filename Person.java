package exercise1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;

import org.joda.time.DateTime;
import org.joda.time.Years;

public class Person {
	private String fName;
	private String lName;
	private Date birthday;
	private ArrayList<Car> cars;
	
	
	public Person(String fName,String lName, String day,String month, String year) {
		super();
		this.fName = fName;
		this.lName = lName;
		this.cars = new ArrayList();
		
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		
		String d = day + month + year;
		
		try {
			Date birthdate = sdf.parse(d);
			this.birthday = birthdate;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			this.birthday = new Date();
		}
	}
	
	public int getAge() {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		Date today = new Date();
		
		DateTime dtBirthDay = new DateTime(birthday);
		DateTime dtToday = new DateTime(today);
		
		int years = Years.yearsBetween(dtBirthDay, dtToday).getYears();
		return years;
	}
	
	public int getValueofCars() {
		int ValueofCars = 0;
		for (Car car : cars) {
			ValueofCars = car.getPrice() + ValueofCars;
		}
		return ValueofCars;
	}
	
	public void addCar(Car c) {
		cars.add(c);
	}
}