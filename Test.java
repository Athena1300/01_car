package exercise1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Test {

	public static void main(String[] args) {

		Producer p1 = new Producer("audi", 0.15f);
		Engine e1 = new Engine("Diesel", 60.5);
		Car c1 = new Car("red", 200, 10000, 10, 50000, p1, e1);
		Person pe1 = new Person("Stefan","M�ller", "15", "03", "2001");

		System.out.println(pe1.getAge());
		pe1.addCar(c1);
		pe1.addCar(c1);
		System.out.println(pe1.getValueofCars());
	}
}
